NAME: Jhory Y. Goco Jr.
NICKNAME: Jygs
MOTIVATION ON JOININIG THE BOOTCAMP:
I always wanted to learn programming that I can use in website creation and design.
Studying was always the plan but I was torn between taking my board exams for ECE, take the Certification for Professional Teaching Program to stay in my current job, or jump to a crash course or another degree related to game dev or software dev.
My good friend from college, an alumnus of Zuitt, was surprisingly thriving in the industry we wanted to apply in but we were not qualified due to our outdated programming language and training. She then recommended me with Zuitt which trained her at home via online classes and managed to get work-from-home setup job with surprisingly good salary.
These are the reasons why I pushed with Zuitt's Bootcamp. 
WORK EXPERIENCE:
I recently graduated BS Electronics Engineering in Far Eastern University - Institute of Technology last March 2020. Since then, I was stuck in the pandemic whilst patiently waiting for DepEd to deploy me for my rendering of service.
Currently, I am a Senior High School teacher in Caruhatan National High School teaching Physical Science and Empowerment Technology to Grade 12 and 11 students, respectively.
Moreover, I was given the special task to be the ICT Coordinator of the Senior High School Department. This allowed me to catch on the in-depth application of MS Excel to various aspects of tasks related to school reports. In addition, since it is still the pandemic, the school thrived to migrate student enrollment and annuoncements to the online world. To act as an aide, we were given basic HTML seminars which is set to goal a 'working school website' by the use of Google Sites.